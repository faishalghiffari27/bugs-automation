const { expect } = require('@wdio/globals')
const { browser } = require('@wdio/globals')

describe('Bug Found on Contact Page', () => {
    it('I can submit Contact Us form with invalid data format', async () => {

        // get inputs & button components
        const inputFullName = $('//*[@id="in_name"]');
        const inputNickname = $("//input[@id='in_nickname']");
        const inputEmail = $("//input[@id='in_email']");
        const inputPhone = $("//input[@id='in_phone']");
        const inputCompany = $("//input[@id='in_institution']");
        const inputHowDidYouFind = $('//*[@id="in_subject"]');
        const inputMessage = $("//textarea[@id='in_message']");
        const btnRecaptcha = $("iframe[title='reCAPTCHA']");
        const btnSubmit = $("//button[@class='card-btn-primary w-2/5']");

        //const for values input
        const valueFullName = "2342322@#$@#%#$"
        const valueNickname = "4857687&^**&(%$"
        const valueEmail = "hayoo67@googo.com"
        const valuePhone = "00000000000000000000"
        const valueCompany = "!#@$%4565576"
        const valueMessage = "7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&"


        // open contact page
        await browser.url(`https://javan.co.id/contact`)


        // do the test
        await inputFullName.setValue(valueFullName);
        await inputNickname.setValue(valueNickname);
        await inputEmail.setValue(valueEmail);
        await inputPhone.setValue(valuePhone);
        await inputCompany.setValue(valueCompany);
        await inputHowDidYouFind.selectByAttribute('value', 'Social Media');
        await inputMessage.setValue(valueMessage);
        await btnRecaptcha.click();
        await btnSubmit.click();

        await expect(inputFullName).toHaveValue(valueFullName)
        await browser.pause(2000)
        await expect(inputNickname).toHaveValue(valueNickname)
        await browser.pause(2000)
        await expect(inputEmail).toHaveValue(valueEmail)
        await browser.pause(2000)
        await expect(inputPhone).toHaveValue(valuePhone)
        await browser.pause(2000)
        await expect(inputCompany).toHaveValue(valueCompany)
        await browser.pause(2000)
        await expect(inputHowDidYouFind).toHaveValue('Social Media')
        await browser.pause(2000)
        await expect(inputMessage).toHaveValue(valueMessage)
        await browser.pause(10000)
        await btnRecaptcha.click()
        await browser.pause(10000)
        await btnSubmit.click()
        await browser.pause(15000)


    })

    // it('I can input full name with numbers', async () => {

})

