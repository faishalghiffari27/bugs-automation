const { expect } = require('@wdio/globals')
const { browser } = require('@wdio/globals')
// const { highlightElement } = require('../helpers/utils')



describe('Bug Design Found on Opportunities', () => {
    it('Different desing wording for all job opportunities and reduced images', async () => {

        // get inputs & button components

        const btnApplyJobAngular = $("//div[@class='flex flex-row flex-wrap justify-around px-0.5 mb-20 aos-init aos-animate']//div[@class='flex justify-center space-x-4']/a[@href='https://javan.co.id/career/angular-developer/54']");
        const btnCareerMenu = $("//ul[@id='mobileMenu']//a[.='Career']");
        const btnApplyJobFreeFED = $("//div[@class='flex flex-row flex-wrap justify-around px-0.5 mb-20 aos-init aos-animate']//div[@class='flex justify-center space-x-4']/a[@href='https://javan.co.id/career/freelance-front-end-developer-reactjs-angular/87']");


        //const for values input

        // const valueFullName = "2342322@#$@#%#$"
        // const valueNickname = "4857687&^**&(%$"
        // const valueEmail = "hayoo67@googo.com"
        // const valuePhone = "00000000000000000000"
        // const valueCompany = "!#@$%4565576"
        // const valueMessage = "7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&"


        // open contact page
        await browser.url("https://javan.co.id/career")


        // do the test
        await btnApplyJobAngular.click();
        await browser.pause(10000)
        await btnCareerMenu.click();
        await browser.pause(10000)
        await btnApplyJobFreeFED.click()
        await browser.pause(10000)

        // await highlightElement("//img[@src='https://javan.co.id/storage/522/JAVA-DEVELOPER.png']")

        // await (btnAngular).click()
        // await browser.pause(10000)
        // await expect(btnBackCareer).click()
        // await browser.pause(10000)
        // await expect(btnFreeFED).click()
        // await browser.pause(10000)


    })

    // it('I can input full name with numbers', async () => {

})

