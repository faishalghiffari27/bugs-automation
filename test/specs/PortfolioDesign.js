const { expect } = require('@wdio/globals')
const { browser } = require('@wdio/globals')

describe('Dropdown on Portfolio', () => {
    it('The year and client portfolio dropdowns overlap when opened at the same time', async () => {

        // get inputs & button components

        const btnTahunPembuatanPortfolio = $("//button[@id='portfolio-btn-tahun-pembuatan']");
        const btnTipeKlien = $("//button[@id='portfolio-btn-jenis-sistem']");

        //const for values input

        // const valueFullName = "2342322@#$@#%#$"
        // const valueNickname = "4857687&^**&(%$"
        // const valueEmail = "hayoo67@googo.com"
        // const valuePhone = "00000000000000000000"
        // const valueCompany = "!#@$%4565576"
        // const valueMessage = "7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&7364856348756387465$#%^$%^$$&^%*&^^*^^*^*&%^&%^&"


        // open contact page
        await browser.url("https://javan.co.id/portfolio")


        // do the test

        await btnTahunPembuatanPortfolio.click()
        await browser.pause(10000)
        await btnTipeKlien.click()
        await browser.pause(10000)



    })

    // it('I can input full name with numbers', async () => {

})

