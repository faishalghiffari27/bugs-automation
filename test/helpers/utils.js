const { browser } = require('@wdio/globals')


browser.addCommand('highlightElement', function (element) {
    browser.execute('arguments[0].style.backgroundColor = "#FDFF47";', element);//provide a yellow background 
    browser.execute('arguments[0].style.outline = "#f00 solid 4px";', element); //provide a red outline
});

export const highlightElement = browser.highlightElement(element);

